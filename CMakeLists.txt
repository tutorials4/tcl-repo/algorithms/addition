cmake_minimum_required (VERSION 3.10)

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId} VERSION 1.0.0)

message(STATUS "PROJECT: ${ProjectId} ${PROJECT_VERSION}")

### LIBRARY
set(${ProjectId}_SOURCES
    src/add.cpp
)

add_library(${ProjectId} STATIC ${${ProjectId}_SOURCES})
target_include_directories(${ProjectId} PUBLIC include)

set_target_properties(${ProjectId} PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin"
)

### INSTALLATION
install(DIRECTORY include/ DESTINATION include)
install(
  TARGETS ${ProjectId}
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)

### TEST
if (CMAKE_SYSTEM_NAME STREQUAL "Android")
  set(GTEST_DIR ${ANDROID_NDK}/sources/third_party/googletest)
else()
  if (NOT DEFINED GTEST_DIR)
    message(FATAL_ERROR "Enable build test by define GTEST_DIR")
  endif()
  set(TEST_DEPS pthread)
endif()
add_subdirectory(test)

