namespace tcl {
namespace addition {
class Engine {
 public:
  Engine();
  ~Engine();

  int init(int offset);
  void process(int a, int b);

  int result() const;

 private:
  int _result;
  int _offset;
};
}  // namespace addition
}  // namespace tcl
