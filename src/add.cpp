#include "add.hpp"

namespace tcl {
namespace addition {
Engine::Engine() : _result(0), _offset(-1) {}
Engine::~Engine() = default;

int Engine::init(int offset) {
  if (_offset >= 0) return 0;  // already init
  if (offset < 0) return -1;   // wrong argument
  _offset = offset;
  return 1;
}

void Engine::process(int a, int b) {
  if (_offset < 0) return;  // not init
  _result = _offset + a + b;
}

int Engine::result() const { return _result; }

}  // namespace addition
}  // namespace tcl
