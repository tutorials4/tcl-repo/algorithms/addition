#include <gtest/gtest.h>
#include <add.hpp>

class AddEngineTest : public ::testing::Test {
 protected:
  void SetUp() override { EXPECT_EQ(engine.init(1), 1); }
  void TearDown() override {}
  tcl::addition::Engine engine;
  tcl::addition::Engine noInitEngine;
};

TEST_F(AddEngineTest, IncorrectInit) { EXPECT_EQ(noInitEngine.init(-1), -1); }

TEST_F(AddEngineTest, DoubleInit) { EXPECT_EQ(engine.init(1), 0); }

TEST_F(AddEngineTest, NoInitNoProcess) { EXPECT_EQ(noInitEngine.result(), 0); }

TEST_F(AddEngineTest, InitOnly) { EXPECT_EQ(engine.result(), 0); }

TEST_F(AddEngineTest, NoInitButProcess) {
  noInitEngine.process(1, 0);
  EXPECT_EQ(noInitEngine.result(), 0);
}

TEST_F(AddEngineTest, OneAddZeroInput) {
  engine.process(1, 0);
  EXPECT_EQ(engine.result(), 2);
}
